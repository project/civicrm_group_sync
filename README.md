# CiviCRM Group Sync

Syncs members from Drupal group to CiviCRM Groups.

## Installation

- Download and enable this module, ensure you already have Group module https://www.drupal.org/project/group downloaded.
- Add Members to Drupal Group.
- Related CiviCRM contact is added to the civicrm group matching on group label.

## Todo

- Create CiviCRM Group if it does not exist after Drupal Group is created.
- Delete CiviCRM Group on drupal group deletion.
- Provide reverse sync, i.e, add member to drupal group after contact is added to group in CiviCRM.
