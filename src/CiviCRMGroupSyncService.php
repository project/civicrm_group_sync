<?php

namespace Drupal\civicrm_group_sync;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;

class CiviCRMGroupSyncService {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CiviCRMGroupSyncService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Trigger when group membership is added/deleted.
   *
   * @param ContentEntityInterface $entity
   * @param bool $remove
   */
  public function processGroupMembershipChange($entity, $remove = FALSE) {
    if (str_ends_with($entity->bundle(), '-group_membership')) {
      $group_id = $entity->get('gid')->getValue()[0]['target_id'];
      $user_id = $entity->get('entity_id')->getValue()[0]['target_id'];
      $group_entity = \Drupal::entityTypeManager()->getStorage('group')->load($group_id);
      if ($group_entity) {
        $group_name = $group_entity->label();
        $this->syncUserToCiviCRMGroup($user_id, $group_name, $remove);
      }
    }
  }

  /**
   * Syncs a user's group memberships with CiviCRM groups.
   *
   * @param int $user_id
   *   The ID of the user whose memberships to sync.
   * @param string $group_name
   *   The name of the group to sync.
   * @param bool $remove
   *   (Optional) Whether to remove the user from the group. Defaults to FALSE.
   */
  public function syncUserToCiviCRMGroup($user_id, $group_name, $remove = FALSE) {
    $cid = \Civi\Api4\UFMatch::get(FALSE)
      ->addSelect('contact_id')
      ->addWhere('uf_id', '=', $user_id)
      ->execute()
      ->first()['contact_id'] ?? NULL;
    if (!empty($cid) && !empty($group_name)) {
      $exist = \Civi\Api4\GroupContact::get(TRUE)
        ->addWhere('group_id:label', '=', $group_name)
        ->addWhere('contact_id', '=', $cid)
        ->execute()
        ->count();

      if ($exist > 0) {
        $status = $remove ? 'Removed' : 'Added';
        \Civi\Api4\GroupContact::update(FALSE)
          ->addValue('status:name', $status)
          ->addWhere('contact_id', '=', $cid)
          ->addWhere('group_id:label', '=', $group_name)
          // ->addWhere('status:name', '=', 'Added')
          ->execute();
        return;
      }
      elseif (!$remove) {
        \Civi\Api4\GroupContact::create(FALSE)
          ->addValue('group_id:label', $group_name)
          ->addValue('contact_id', $cid)
          ->addValue('status:name', 'Added')
          ->addValue('method', 'API') // None of these 2 currently work in adding the method!
          ->setMethod('API')
          ->execute();
      }
    }
  }

}